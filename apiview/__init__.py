from .swagger_views import swagger
from .blueprint import APIBlueprint
from .views import APIView
from . import views

import logging
from werkzeug.routing import BaseConverter

logger = logging.getLogger(__name__)


from flask import g, request

@swagger.before_request
def pop_api_selection():
    g.api_selection = request.view_args.pop('api_selection', 'api')


class APISelectionConverter(BaseConverter):
    regex = 'api'
    registered_apis = []

    def __init__(self, *args, **kwargs):
        super(APISelectionConverter, self).__init__(*args, **kwargs)

    @classmethod
    def register_api(cls, api):
        cls.registered_apis.append(api)
        cls.regex = '(?:{})'.format('|'.join(cls.registered_apis))


def init_app(app, apis=None):
    """
    Called during Flask app creation
    """
    apis = apis or ['api']
    app.url_map.converters['api_selection'] = APISelectionConverter

    for api in apis:
        APISelectionConverter.register_api(api)

    app.register_blueprint(swagger, url_prefix='/<api_selection:api_selection>')

    json_encoder = app.config.get('JSON_ENCODER')
    if json_encoder:
        logger.info('Injecting JSONEncoder from configuration')
        from json.encoder import JSONEncoder
        assert isinstance(json_encoder, type(JSONEncoder))
        views.JSONEncoder = json_encoder
