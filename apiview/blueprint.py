from flask.blueprints import Blueprint
from . import registry

import logging
logger = logging.getLogger(__name__)


class APIBlueprint(Blueprint):
    """
    Register this blueprint as an API in Swagger
    """
    def __init__(self, name, import_name, description=None, api_name='api', *args, **kwargs):
        kwargs.setdefault('url_prefix', '/')  # always set URL prefix
        self.api_name = api_name
        super(APIBlueprint, self).__init__(name, import_name, *args, **kwargs)

        if name in registry.Blueprints:
            logger.error('%s already in APIBlueprint registry!', name)
        else:
            registry.Blueprints[name] = self

            resource = {
                'path': '/api-docs.json/{}'.format(self.name)
            }

            if description:
                resource['description'] = description


            registry.Swagger_Resource_Listing[api_name]['apis'].append(resource)


    def __repr__(self):
        return '<APIBlueprint({}, {})>'.format(repr(self.name), repr(self.import_name))