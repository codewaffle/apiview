import json
from wtforms import widgets, ValidationError
from wtforms.ext.dateutil.fields import DateTimeField
from wtforms.fields.core import Field

try:
    import arrow
except ImportError:
    arrow = None


class JSONField(Field):
    widget = widgets.TextInput()

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = valuelist[0]
        else:
            self.data = {}

        if not isinstance(self.data, dict):
            self.data = json.loads(self.data)

    def _value(self):
        return json.dumps(self.data)


class SmartDateTimeField(DateTimeField):
    def process_formdata(self, valuelist):
        if arrow is None:
            return super(SmartDateTimeField, self).process_formdata(valuelist)

        if valuelist:
            date_str = ' '.join(valuelist)
            if not date_str:
                self.data = None
                raise ValidationError(self.gettext('Please input a date/time value'))

            try:
                self.data = arrow.get(date_str).to('utc').naive
            except ValueError:
                self.data = None
                raise ValidationError(self.gettext('Invalid date/time input'))
