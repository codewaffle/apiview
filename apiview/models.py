from functools import wraps
from uuid import UUID
from flask import request, flash
from werkzeug.datastructures import ImmutableMultiDict
import wtforms
import json

import logging
from wtforms.fields.core import FieldList, FormField

logger = logging.getLogger(__name__)

try:
    import mongoengine
except ImportError:
    logger.info("MongoEngine (optional) not found")
    mongoengine = None

try:
    from flask.ext.mongoengine.wtf import model_form
except ImportError:
    logger.info("Flask-MongoEngine (optional) not found")
    model_form = None


def _default_filter(s):
    if isinstance(s, UUID):
        return str(s)

    return s

def dict_to_form(data, input_model, output_filter=_default_filter):
    """
    Transform a JSON-derived dict into form_data consumable by WTForms

    example JSON::
        {
            "form_field": {
                "sub_a": "value",
                "some_id": 123
            },
            "int_field_list": [1, 2, 3],
            "form_field_list": [
                {"sub_a": "value_0"},
                {"sub_a": "value_1"}
            ]
        }

    """
    items = []
    form = input_model()

    for kw, arg in data.iteritems():
        field = form[kw] if kw in form else None

        # replace Nones with blank values
        if arg is None:
            arg = ''
        elif isinstance(arg, (list, tuple)):
            if isinstance(field, FieldList):
                if issubclass(field.unbound_field.field_class, FormField):
                    # we're expecting a list of dictionaries
                    for idx, sub_data in enumerate(arg):
                        assert isinstance(sub_data, dict), '{}: expecting dict, got {}'.format(kw, type(sub_data))

                        for sub_kw, sub_val in sub_data.items():
                            items.append(('{}-{}-{}'.format(kw, idx, sub_kw), sub_val))
                else:
                    # we're expecting a list of values
                    items += [
                        ('-'.join((kw, idx)), val) for idx, val in enumerate(arg)
                    ]

                continue  # keep old behaviour if not handled by new behaviour

            items += [(kw, v) for v in arg]  # old behaviour
        elif isinstance(arg, (dict, )) and isinstance(field, FormField):
            items += [
                ('{}-{}'.format(kw, sub_kw), sub_val) for sub_kw, sub_val in arg.items()
            ]
        else:
            items.append((kw, arg))

    return ImmutableMultiDict((a, output_filter(b)) for a,b in items)


def APIParameters(input_model, output_model=None, attach_form_data=False):
    """
    WTForms-based validation for endpoint input and output
    """
    if model_form is not None:
        if isinstance(input_model, type(mongoengine.Document)):
            input_model = model_form(input_model)

        if isinstance(output_model, type(mongoengine.Document)):
            output_model = model_form(output_model)

    def wrapper(fn):
        fn.input_model = input_model
        fn.output_model = output_model



        @wraps(fn)
        def inner(*args, **kwargs):
            if request.method not in ['DELETE'] and request.data:  # we (should) have received JSON instead of a form
                data = json.loads(request.data)
                form_data = dict_to_form(data, input_model)
            elif request.form:
                form_data = request.form
            elif request.args:
                form_data = request.args
            else:
                logger.warning('No Form Data!')
                form_data = None

            form = input_model(form_data, csrf_enabled=False)

            # Gotta do our own WTForms->Flask adapter - attach files to form fields
            if request.files:
                for k, v in request.files.iteritems():
                    getattr(form, k).data = v

            if not form.validate():
                assert form.errors, 'Form failed to validate but no errors were reported'
                flash("There was a problem with your form submission.", 'error')
                return form.errors, 400

            kwargs['form'] = form

            if attach_form_data:
                kwargs['form_data'] = form_data

            result = fn(*args, **kwargs)

            if output_model:
                assert isinstance(result, dict), "Output is not a dict"
                output_data = dict_to_form(result, output_model)
                output_form = output_model(output_data, csrf_enabled=False)

                if not output_form.validate():
                    return output_form.errors, 500

            return result

        return inner

    return wrapper


def InputModel(model):
    """
    Deprecated - for backwards compatibility
    """
    logger.warning('Deprecated - use @APIParameters')
    return APIParameters(input_model=model)


class APIParameterForm(wtforms.Form):
    """WTForms-based object model for input/output"""
    def write(self, *args, **kwargs):
        raise NotImplementedError

APIModel = APIParameterForm
