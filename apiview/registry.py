from collections import defaultdict
import logging


logger = logging.getLogger(__name__)

def default_resource_listing():
    return {
        'apiVersion': 'dev',
        'swaggerVersion': '1.1',
        'apis': []
    }

Swagger_Resource_Listing = defaultdict(default_resource_listing)

Swagger_API_Declarations = defaultdict(dict)


def NewSwaggerDeclaration(resourcePath):
    return {
        'apiVersion': 'dev',
        'swaggerVersion': '1.1',
        'resourcePath': resourcePath,
        'apis': []
    }

Blueprints = {}