import os
from flask import Blueprint, Response, render_template, request, current_app, redirect, url_for
import json

from . import registry
from flask.ext.login import login_required
from flask import g

swagger = Blueprint(
    name='swagger',
    import_name=__name__,
    template_folder='templates',
    static_folder='swagger_static',
    static_url_path='/api-docs/static'  # relative to url_prefix
)


@swagger.route('/api-docs/')
@login_required
def swagger_index():
    return render_template(
        ['swagger_index_{}.html'.format(g.api_selection), 'swagger_index.html'],
        config=current_app.config,
        request=request,
        dump=json.dumps,
        api_name=g.api_selection
    )


@swagger.route('/api-docs')
@login_required
def swagger_index_redirect():
    return redirect(url_for('.swagger_index'))


@swagger.route('/api-docs.json')
def swagger_resource_listing():
    data = registry.Swagger_Resource_Listing[g.api_selection].copy()
    data['basePath'] = request.host_url + g.api_selection

    data['apis'] = sorted(data['apis'], key=lambda a: a['path'])

    response = Response(
        json.dumps(data),
        status=200,
        mimetype='application/json'
    )

    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE'

    return response


@swagger.route('/services.json')
def services_json():

    apis = []

    for k, v in registry.Swagger_API_Declarations[g.api_selection].iteritems():
        for _api in sorted(v['apis'], key=lambda a: a['path']):
            api = _api.copy()
            for op in api['operations']:
                op['parameters'].append({
                    'name': 'user_key',
                    'description': 'user key',
                    'dataType': 'string',
                    'required': True,
                    'paramType': 'query'
                })

            apis.append(api)

    if '3SCALE_PROXY_BASE' in os.environ:
        basePath = os.environ['3SCALE_PROXY_BASE'] + '/' + g.api_selection
    else:
        basePath = request.host_url + g.api_selection

    data = {
        'apiVersion': 'dev',
        'apis': apis,
        'basePath': basePath
    }

    return Response(
        json.dumps(data),
        status=200,
        mimetype='application/json'
    )



@swagger.route('/api-docs.json/<name>')
def swagger_docs(name):
    try:
        data = registry.Swagger_API_Declarations[g.api_selection][name].copy()
    except KeyError:
        data = {}

    data['apis'] = sorted(data['apis'], key=lambda a: a['path'])

    return Response(
        json.dumps(data),
        status=200,
        mimetype='application/json'
    )
