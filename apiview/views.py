import json
from json.encoder import JSONEncoder as DefaultJSONEncoder
import logging

from flask import request, current_app, Response, session, message_flashed, flash
from flask.globals import _app_ctx_stack
from flask.views import MethodView, MethodViewType
from six import text_type
from werkzeug.wrappers import BaseResponse
from wtforms.fields.core import UnboundField
from wtforms import HiddenField
import re


logger = logging.getLogger(__name__)


from . import registry

JSONEncoder = DefaultJSONEncoder
JSONIndent = 4


def dumps(data, **kwargs):
    kwargs.setdefault('indent', current_app.config.get('JSON_INDENT', JSONIndent))
    kwargs.setdefault('cls', JSONEncoder)
    return json.dumps(data, **kwargs)


def method_nickname(cls, *args):
    # method = hex(id(cls))
    return '.'.join((cls.__module__, cls.__name__) + args).replace('.', '-')


swagger_map = {
    'IntegerField': 'int',
    'StringField': 'string',
    'TextAreaField': 'string',
    'SelectField': 'string',
    'QuerySelectField': 'string',
    'EmailField': 'string',
    'CSRFTokenField': 'string',
    'ModelSelectField': 'string',
    'DateField': 'string',
    'SmartDateField': 'string',
    'DateTimeField': 'string',
    'SmartDateTimeField': 'string',
    'BooleanField': 'boolean',
    'PasswordField': 'string',
    'JSONField': 'string',

    'DecimalField': 'float',
    'FloatField': 'float',

    'FileField': ('file', 'body'),

    'FieldList': 'array',
    'FormField': 'string',

    'int': 'int',
    'string': 'string',
    'path': 'string',
    'float': 'float',
    'uuid': 'string'
}


# TODO: spider more __doc__ stuff and make it available in Swagger
def swagger_ops_from_cls(cls):
    ops = []

    url_rule = cls.url_rule or ''

    params = url_rule.split('<')
    params = params[1:]  # first entry is junk

    path_params = []

    for p in params:
        param, junk = p.split('>')

        if ':' in param:
            ptype, param = param.split(':')
        else:
            ptype = 'string'

        path_params.append({
            'position': len(path_params),
            'name': param,
            'dataType': swagger_map[ptype],
            'allowMultiple': False,
            'paramType': 'path',
            'required': True
        })

    for method in cls.methods:
        if method in ['OPTIONS', ]:
            continue  # skip autodocumenting options

        fn = getattr(cls, method.lower())

        op = {
            'httpMethod': method,
            'nickname': method_nickname(cls, method),
            'summary': (fn.__doc__ or '').strip(),
            'parameters': path_params[:]
        }

        if hasattr(fn, 'notes'):
            op['notes'] = fn.notes

        if hasattr(fn, 'input_model') and fn.input_model:
            form = fn.input_model

            form = form(csrf_enabled=False)

            for field_name, member in form._unbound_fields:
                if not isinstance(member, UnboundField) or issubclass(member.field_class, HiddenField):
                    continue

                # bind the field so we can inspect it
                form_args = member.args
                form_kwargs = member.kwargs.copy()
                form_kwargs.update({
                    '_form': form,
                    '_name': field_name
                })
                field = member.field_class(*form_args, **form_kwargs)

                swag = swagger_map.get(field.type, 'string')

                if isinstance(swag, tuple):
                    dataType, paramType = swag
                else:
                    dataType = swag

                    if method in ('GET', 'DELETE'):
                        paramType = 'query'
                    else:
                        paramType = 'form'

                param = {
                    'position': member.creation_counter + 100,
                    'name': field.name,
                    'description': str(field.description) or str(field.label.text),
                    'dataType': dataType,
                    'allowMultiple': False,
                    'required': field.flags.required,
                    'paramType': paramType
                }

                if field.type == 'SelectField' and field.choices is not None:
                    param['allowableValues'] = {
                        'valueType': 'LIST',
                        'values': [c[0] for c in field.choices]
                    }

                op['parameters'].append(param)

        op['parameters'] = sorted(op['parameters'], key=lambda x: x['position'])
        ops.append(op)

    return ops

# Metaprogramming below here to make Swagger integration feel more magical


class APIViewMeta(MethodViewType):
    def __new__(mcs, name, args, kwargs):
        mcs = MethodViewType.__new__(mcs, name, args, kwargs)

        if not mcs.methods:
            return mcs

        if not mcs.blueprint:
            return mcs


        # group on blueprint name
        resourcePath = mcs.blueprint.url_prefix

        apiName = mcs.blueprint.api_name

        bpName = mcs.blueprint.name

        if bpName not in registry.Swagger_API_Declarations[apiName]:
            registry.Swagger_API_Declarations[apiName][bpName] = registry.NewSwaggerDeclaration(bpName)

        api_decl = registry.Swagger_API_Declarations[apiName][bpName]

        url_rule = mcs.url_rule or ''

        swagger_rule = url_rule.replace('<', '{').replace('>', '}')
        for t in swagger_map.keys():
            # lazy way to strip out Flask types
            swagger_rule = swagger_rule.replace("{}:".format(t), "")

        API = {
            'path': resourcePath + swagger_rule,
            'description': (mcs.__doc__ or '').strip(),
            'operations': swagger_ops_from_cls(mcs)
        }

        api_decl['apis'].append(API)

        nickname_args = [mcs]
        nickname_args.extend(kwargs.get('nickname_extra', []))
        if len(nickname_args) > 1:
            print 'ok'

        mcs.blueprint.add_url_rule(mcs.url_rule, view_func=mcs.as_view(method_nickname(*nickname_args)))

        return mcs

status_code_re = re.compile(r'^(\d*):?(.*)')

class APIView(MethodView):
    __metaclass__ = APIViewMeta
    blueprint = None
    url_rule = ''
    swagger_rule = None

    def options(self, *args, **kwargs):
        # TODO: make this return actual options
        """OPTIONS requests don't need a body - they just need headers from dispatch_request"""
        return None

    def dispatch_request(self, *args, **kwargs):
        logger.debug('APIView.dispatch_request(%s, %s)', repr(args), repr(kwargs))

        cors_headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE'
        }

        if 'Access-Control-Request-Headers' in request.headers:
            cors_headers['Access-Control-Allow-Headers'] = request.headers['Access-Control-Request-Headers']

        messages = []

        def capture_flashes(*args, **kwargs):
            flashes = session['_flashes']
            category, message = flashes.pop() if flashes else (kwargs.get('category'), kwargs.get('message'))
            messages.append({'category': category, 'message': message})
            session['_flashes'] = flashes
        message_flashed.connect(capture_flashes)

        try:
            result = super(APIView, self).dispatch_request(*args, **kwargs)
        except Exception as E:
            logger.exception("Exception while trying to dispatch a request")
            response_data = {'error': text_type(E)}
            error_message = getattr(E, 'description',
                text_type(E.args[0]) if E.args else "An unknown error occurred.")

            if hasattr(E, 'code'):
                status_code = E.code
            else:
                if re.match(r'\d{3}\b', error_message):
                    status_code = int(error_message[:3])
                    error_message = error_message[3:].strip()
                else:
                    status_code = 500

            if str(status_code)[0] == '5' and hasattr(current_app, 'extensions') and 'sentry' in current_app.extensions:
                response_data['error_ref'] = current_app.extensions['sentry'].captureException()

            response_data['message'] = error_message  # deprecated

            flash(error_message, 'error')
            response_data['messages'] = messages
            response = Response(
                dumps(response_data),
                status=status_code,
                mimetype='application/json')
            response.headers.extend(cors_headers)
            return response
        finally:
            message_flashed.disconnect(capture_flashes)

        if isinstance(result, BaseResponse):
            # Return directly - we know what we're doing
            result.headers.extend(cors_headers)
            return result


        extra = {}

        status = 200

        if isinstance(result, tuple):
            # Parse tuple out into our result
            for x in result[1:]:
                if isinstance(x, dict):
                    extra.update(x)
                elif isinstance(x, int):
                    status = x
                else:
                    logger.error("Something went wrong, return value unexpected")
            result = result[0]
            # end tuple parse

        if current_app.config.get('API_SIMPLE_RESPONSE'):
            response_data = result
        else:
            response_data = {
                'status': status,
                'result': result
            }

            if extra is not None:
                response_data.update(extra)

            if messages:
                response_data['messages'] = messages

        json_response = dumps(response_data)

        # JSONP support
        if request.args.get('callback'):
            json_response = '{callback}({json_response})'.format(callback=request.args['callback'], json_response=json_response)

        response = Response(
            json_response,
            status=status,
            mimetype='application/json'
        )
        response.headers.extend(cors_headers)
        return response
