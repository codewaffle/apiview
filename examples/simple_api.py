import logging
logging.basicConfig(level=logging.DEBUG)

from flask import Flask
from wtforms.fields.core import IntegerField, StringField
from wtforms.validators import Required
from apiview.blueprint import APIBlueprint
from apiview.models import APIParameters, APIModel
from apiview.views import APIView
from apiview import swagger

app = Flask(__name__)

simple_api_view = APIBlueprint('simple_api', __name__, url_prefix='/simple')

mostly_simple = APIBlueprint('mostly_simple', __name__, url_prefix='/mostly/simple')


class SimpleInputModel(APIModel):
    a_number = IntegerField('a number')
    a_string = StringField('a required string field', [Required()])


class SimpleAPI(APIView):
    blueprint = simple_api_view
    url_rule = ''

    def get(self):
        """
        Returns { "status": "OK" }
        """
        return {
            'status': 'OK'
        }

    @APIParameters(SimpleInputModel)
    def post(self, form):
        """
        Validates input using WTForms/InputModel and spits it back out
        """

        return {
            'number': form.a_number
        }
app.register_blueprint(swagger, url_prefix='/api')
app.register_blueprint(simple_api_view, url_prefix='/api' + simple_api_view.url_prefix)

if __name__ == '__main__':
    print app.url_map
    print 'Navigate to http://127.0.0.1:5000/docs'
    app.run()