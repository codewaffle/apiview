"""
APIView provides class-based views with Swagger integration
"""

from setuptools import find_packages, setup

install_reqs = [
    'Flask',
    'WTForms',
    'Flask-Login',
    'blinker'
]

setup(
    name='APIView',
    version='0.1.4',
    description='Flask/Swagger class-based Views',
    long_description=__doc__,
    license='BSD',
    author='Patrick Griffus',
    author_email='patrick@codewaffle.com',
    url='https://github.com/codewaffle/apiview',
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    classifiers=['Programming Language :: Python :: 2.7'],
    install_requires = install_reqs
)
